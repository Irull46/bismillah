import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {Component} from "react";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      input1 : '',
      input2 : '',
      cetak : ''
    };
  };

  gabungInput = () => {
    const input1 = this.state.input1;
    const input2 = this.state.input2;
    if (input1 && input2 != '') {
      this.setState({cetak: `Nama Lengkap : ${input1} ${input2}`});
    }
  };

  render () {
    return (
      <View style={gaya.bungkus}>
        <TextInput
          style={gaya.input}
          placeholder={"Input nama depan"}
          onChangeText={(input1) => this.setState({input1})}
        />
        <TextInput
          style={gaya.input}
          placeholder={"Input nama depan"}
          onChangeText={(input2) => this.setState({input2})}
        />

        <Text style={gaya.cetak}>
          {this.state.cetak}
        </Text>

        <TouchableOpacity style={gaya.tombol} onPress={() => this.gabungInput()}>
          <Text style={gaya.fonttombol}>
            KLIK DI SINI
          </Text>
        </TouchableOpacity>
      </View>
    );
  };
};

export default App;

const gaya = StyleSheet.create({
  bungkus: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tombol: {
    backgroundColor: 'yellow',
    padding: 12,
    borderRadius: 5,
    margin: 10
  },
  input: {
    backgroundColor: 'black',
    margin: 5,
    width: '90%',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: 'white'
  },
  cetak: {
    fontSize: 20
  },
  fonttombol: {
    color: 'black'
  }
});